#!/usr/bin/bash.exe
comment=$1
git add Playbooks/* roles/* group_vars/*
git commit -m "$comment"
git push origin master
vagrant provision ansible --provision-with "pull_repo"
